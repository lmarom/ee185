# Assignment 3: The Python/C Boundary

*Written by Philip Levis*

**Due: Thursday, October 17, 2019 at 10:00 PM**

## Goals ##

By the end of this assignment, you'll have

  - Made the NeoPixel library faster,
  - Installed new CircuitPython firmware,
  - Learned exactly how Python calls into C,
  - Changed the Python bindings to C code, and
  - Learned about the challenges of improving performance.

## A Better NeoPixel library ##

You may do this assignment in a group of 2. If you do, you only need to send one
email to hand in, and say in the email who the other person in the group was, as well
as a rough assessment of who did what.

In Assignment 2, you saw how the standard NeoPixel library is extremely
slow. When you set a pixel in the NeoPixel array, this executes a lot of
(slow) Python code. You saw in Assignment 1 that Python is a 200-400x slowdown
over C; this means your 140MHz processor acts more like a 350-700kHz processor,
so it's like programming a 40-year-old [Tangering Microtan 65](https://en.wikipedia.org/wiki/Tangerine_Microtan_65).

In this assignment, you're going to write a new interface to the NeoPixels
that doesn't have this problem. Rather than do its processing in Python,
all of the processing is done in C code. One approach to doing this is to
change the the `__setitem__` interface, so that this Python code calls into
C code. This will start to get a bit complicated. We'd need to decide whether
the pixels are stored in C (so `__getitem__` calls a C function), or whether
we have copies in C and Python. 

Another approach, which is simpler, is to use something called *lazy
evaluation.* Rather than figure out the bits to send each pixel when that
pixel is assigned to (i.e., the long `__setitem__` function), the code
will just store the color value. Then, when it has to actually write
the bits to send (including things like the global brightness setting),
it will compute the bits for all of the pixels at once and send them out.
This approach is called lazy evaluation because it doesn't do any work
until the last possible moment. In cases when code assigns to the same pixel
50 times before calling `show()`, lazy evaluation will only compute the 
pixel's actual value once.

### Setting up your environment to build CircuitPython ###

Changing the interface CircuitPython requires compiling a new CircuitPython
firmware image and installing it on your board. 
[Adafruit has a web page](https://learn.adafruit.com/building-circuitpython/introduction) 
with instructions for doing this under Windows, Linux and Mac OSX. The whole
CircuitPython distribution will require about 2GB of disk space. So, for
example, in the OS X instructions, when it tells you to create a disk volume,
create one that's 2GB in size. *Follow their instructions exactly!* I found
that trying to do things in a custom way quickly devolved into lots of 
problems that were not easy to solve.

### Testing that you can build and install CircuitPython firmware ##

Once you've installed CircuitPython and built `mpy-cross`, you want to
test that everything is working. Change to the `ports/atmel-samd` directory.
Type

`$ make BOARD=feather_m4_express`

If this works correctly, you should see output like this:

```
Use make V=1, make V=2 or set BUILD_VERBOSE similarly in your environment to increase build verbosity.
GEN build-feather_m4_express/genhdr/moduledefs.h
QSTR updated
arm-none-eabi-gcc -DCIRCUITPY_FULL_BUILD=1 -DCIRCUITPY_ANALOGIO=1 -DCIRCUITPY_AUDIOBUSIO=1 -DCIRCUITPY_AUDIOIO=1 -DCIRCUITPY_AUDIOIO_COMPAT=1 -DCIRCUITPY_AUDIOPWMIO=0 -DCIRCUITPY_AUDIOCORE=1 -DCIRCUITPY_AUDIOMIXER=1 -DCIRCUITPY_BITBANGIO=1 -DCIRCUITPY_BLEIO=0 -DCIRCUITPY_BOARD=1 -DCIRCUITPY_BUSIO=1 -DCIRCUITPY_DIGITALIO=1 -DCIRCUITPY_DISPLAYIO=1 -DCIRCUITPY_FREQUENCYIO=1 -DCIRCUITPY_GAMEPAD=1 -DCIRCUITPY_GAMEPADSHIFT=0 -DCIRCUITPY_I2CSLAVE=1 -DCIRCUITPY_MATH=1 -DCIRCUITPY_MICROCONTROLLER=1 -DCIRCUITPY_NEOPIXEL_WRITE=1 -DCIRCUITPY_NETWORK=1 -DCIRCUITPY_NVM=1 -DCIRCUITPY_OS=1 -DCIRCUITPY_PIXELBUF=1 -DCIRCUITPY_PULSEIO=1 -DCIRCUITPY_PS2IO=1 -DCIRCUITPY_RANDOM=1 -DCIRCUITPY_ROTARYIO=1 -DCIRCUITPY_RTC=1 -DCIRCUITPY_SAMD=1 -DCIRCUITPY_STAGE=0 -DCIRCUITPY_STORAGE=1 -DCIRCUITPY_STRUCT=1 -DCIRCUITPY_SUPERVISOR=1 -DCIRCUITPY_TIME=1 -DCIRCUITPY_TOUCHIO_USE_NATIVE=0 -DCIRCUITPY_TOUCHIO=1 -DCIRCUITPY_UHEAP=0 -DCIRCUITPY_USB_HID=1 -DCIRCUITPY_USB_MIDI=1 -DCIRCUITPY_PEW=0 -DCIRCUITPY_USTACK=0 -DCIRCUITPY_BITBANG_APA102=0 -DQSPI_FLASH_FILESYSTEM=1 -DEXPRESS_BOARD -DEXTERNAL_FLASH_DEVICES=GD25Q16C -DEXTERNAL_FLASH_DEVICE_COUNT=1 -DUSB_AVAILABLE -DLONGINT_IMPL_MPZ -Os -DNDEBUG -DCFG_TUSB_MCU=OPT_MCU_SAMD51 -DCFG_TUD_MIDI_RX_BUFSIZE=128 -DCFG_TUD_CDC_RX_BUFSIZE=256 -DCFG_TUD_MIDI_TX_BUFSIZE=128 -DCFG_TUD_CDC_TX_BUFSIZE=256 -DCFG_TUD_MSC_BUFSIZE=1024 -flto -flto-partition=none -I. -I../.. -I../lib/mp-readline -I../lib/timeutils -Iasf4/samd51 -Iasf4/samd51/hal/include -Iasf4/samd51/hal/utils/include -Iasf4/samd51/hri -Iasf4/samd51/hpl/core -Iasf4/samd51/hpl/gclk -Iasf4/samd51/hpl/pm -Iasf4/samd51/hpl/port -Iasf4/samd51/hpl/rtc -Iasf4/samd51/hpl/tc -Iasf4/samd51/include -Iasf4/samd51/CMSIS/Include -Iasf4_conf/samd51 -Iboards/feather_m4_express -Iboards/ -Iperipherals/ -Ifreetouch -I../../lib/tinyusb/src -I../../supervisor/shared/usb -Ibuild-feather_m4_express -I../../drivers/wiznet5k -Wall -Werror -std=gnu11 -nostdlib -fsingle-precision-constant -fno-strict-aliasing -Wdouble-promotion -Wno-endif-labels -Wstrict-prototypes -Werror-implicit-function-declaration -Wfloat-equal -Wundef -Wshadow -Wwrite-strings -Wsign-compare -Wmissing-format-attribute -Wno-deprecated-declarations -Wnested-externs -Wunreachable-code -Wcast-align -Wno-error=lto-type-mismatch -D__SAMD51J19A__ -ffunction-sections -fdata-sections -fshort-enums -DCIRCUITPY_SOFTWARE_SAFE_MODE=0x0ADABEEF -DCIRCUITPY_CANARY_WORD=0xADAF00 -DCIRCUITPY_SAFE_RESTART_WORD=0xDEADBEEF --param max-inline-insns-single=500 -DFFCONF_H=\"lib/oofatfs/ffconf.h\" -DMICROPY_PY_WIZNET5K=5500 -D_WIZCHIP_=5500  -mthumb -mabi=aapcs-linux -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DSAMD51 -DMICROPY_PY_NETWORK=1   -c -o build-feather_m4_express/autogen_display_resources.o build-feather_m4_express/autogen_display_resources.c

223788 bytes free in flash out of 499712 bytes ( 488.0 kb ).
178984 bytes free in ram for stack out of 196608 bytes ( 192.0 kb ).

Converting to uf2, output size: 551936, start address: 0x4000
Wrote 551936 bytes to build-feather_m4_express/firmware.uf2.
``` 

This tells you that you now have a new CircuitPython iamge in 
`build-feather_m4_express/firmware.uf2`. You need to copy this to the Feather board 
to reprogram its firmware. [This webpage on Adafruit](https://learn.adafruit.com/adafruit-feather-m0-express-designed-for-circuit-python-circuitpython/uf2-bootloader-details) tells you how.
The basic summary is that you hit the reset button twice quickly to tell the board to
go into bootloader mode. You'll see that it appears as a different volume, called FEATHERBOOT.
Copy `firmware.uf2` to this volume. The board should reboot with your image and run
normally.

Now you need to test that it's actually reprogramming. Take one of your Python scripts
that cycles the LED colors and install it so you see the LEDs cycling. You're going to
break the NeoPixel LED driver so it doesn't work and install a CircuitPython image with it.
If you are successful, you'll see the LEDs stop working.

Open `ports/atmel-samd/common-hal/neopixel_write/__init__.c`. On line 59, you should find
the function `common_hal_neopixel_write`. Recall this is the function that Python calls
when you call `show()` from Python. It manually times the bits it sends to the NeoPixels.

Change line 100 `pinMask =  (1UL << (pin % 32));` to be `pinMask = 0;`. Be sure to
keep a commented-out version of the original line. This breaks the driver. `pinMask`
holds which GPIO pin to send the bits out on, stored as a bit. By setting it to 0, 
the driver will send bits to no GPIO pin, so will always send 0 and not reprogram LEDs.

Recompile your firmware (from `ports/atmel-samd`) and install it. You should see that the
LEDs stop updating. Next, restore the driver to its original implementation (i.e., fix it),
recompile and reinstall. You should see the LEDs working again.

If you mess up your copy of `__init__.c` or any other file, you can restore it by typing 
`git checkout <filename>`. You can see if you've changed any files by tying `git status`.

### Changing the API


The complexity of the NeoPixel Python code comes from four ways it tries to be helpful:
  1. Python requires that `__setitem__` can pass a slice, not just a single value. That is,
     you can assign with `pixels[3:10] = vals[0:7]`. This is a lot more efficient than
     individual assignments, but still isn't that fast because the driver still calls the
     slow implementation of `_set_item` on each element.
  1. It provides multiple ways that a programmer can assign to a pixel: you can assign the
     pixel as both a 24-bit value as well as an `(R, G, B)` tuple. Having these options
     requires that Python determine what type the input is and branch (an if statement)
     based on the type.
  1. It tries to be smart in a couple of ways, such as if it sees RGB are all the same value
     and there is a while LED, it converts the even RGB into the white pixel.
  1. Finally (and this is less egregious), it does the brightness adjustment in Python when
     `show()` is called. This is good that it's lazy evaluation. But it's in Python.

Unfortunately, Python requires the first one. But you can make things faster by
fixing the other 3.  You're going to change the API in three ways, to fix these problems:

  1. Make `_set_item` take either an integer, or a tuple, but not both (up to you).
  1. Cut out all of the white pixel logic.
  1. Have the C API take the brightness parameter and pass it explicitly, so the C code 
     does the adjustment.

Given that you're simultaneously messing with both C and Python, and the C code is going to
be hard to debug, you'll do the last one first. That way, you're just making a tiny change
to the Python code and mostly changing the C code. If something goes wrong, you know it's 
in your C code, since the Python code is mostly unchanged. Once you're sure that brightness is
being correctly computed, you can then improve the Python API.

In each case, you're going to measure performance before and after, to see how much of
a difference your change makes.

*If messing around with the low-level implementations in CircuitPython seems daunting,
you can jump ahead to the Simplifying Python section first, which involves changing
the Python libraries to solve problems 1 and 2. Spend ~2 hours there before trying to 
improve CircuitPython to solve problem 3.*

### How Python calls C

The Python interpreter defines how it expects Python objects to be laid out in memory.
When you create integers, strings, maps, objects, or other types in Python, the interpreter
allocates memory for them and sets their appropriate fields. Every Python object has a few
bits in it which say what type it is, e.g., if it's a small integer, a float, or an object.
So the interpreter can check these bits to see what type the Python object is.

Because objects created in Python look a certain way in memory, the way that CircuitPython
exports C functions to Python is to just make objects that look as the interpreter expects.
The software layering looks like this:

![Python/CircuitPython layering](stack.png)

At the top level is your Python code. This code calls library functions such as `show()` on
a NeoPixel object. This library functions are a small amount of Python code wrapped around
a binding function implemented inside the CircuitPython runtime, such as 
`neopixel_write_neopixel_write_()`. It's called a binding function because it binds Python
and C code together. The arguments to `neopixel_write_neopixel_write_()` are Python objects
as the interpreter sees them. This function checks that they're the right types of objects,
then converts them into their underlying C objects (e.g., turns a Python float object into
just a 32-bit floating point number) before calling a chip-specific implementation of the
function that actually does the operation, `common_hal_neopixel_write()`. This function
is standard C code, and actually sends bits to NeoPixel LEDs.

### Fix 3: Pushing brightness into C 

Pushing brightness into C has four parts: 
  1. changing the C API definition of `common_hal_neopixel_write()` so it includes a
     brightness parameter, 
  1. changing the implementation of `common_hal_neopixel_write()` to use this brightness 
     value,
  1. updating the bindings between Python and `neopixel_write_neopixel_write_()`
     so it takes a float and passes it to ``common_hal_neopixel_write()`.`
  1. changing the Python code so it passes brightness rather than processing it.


Before we get started, let's see how long it takes to update our LEDs:

1. Write a loop that color-cycles 100 LEDs with a brightness of 0.5. How long does it
   take the loop to execute? You can measure with `time.monotonic()`.

If we want to have the C code perform the brightness computation, we need to pass brightness
as a parameter to `common_hal_neopixel_write`. Open `shared/bindings/neopixel_write/__init__.h`
and add `float brightness` as a parameter to the function declaration.

Next, open `ports/atmel-samd/common-hal/neopixel_write/__init__.c`. Add the brightness
parameter to `common_hal_neopixel_write`. At the top of the
function, before the call to `wait_until`, cycle through the pixels and multiply them
by brightness. Be careful with types -- be sure to cast your result back to a `uint8_t`.
Also, be sure to check that brightness is in the range of 0-1, and return early if it isn't.

Try compiling your image. You should see errors like these:

```
QSTR not updated
../../supervisor/shared/rgb_led_status.c: In function 'new_status_color':
../../supervisor/shared/rgb_led_status.c:183:9: error: too few arguments to function 'common_hal_neopixel_write'
         common_hal_neopixel_write(&status_neopixel, status_neopixel_color, 3);
         ^~~~~~~~~~~~~~~~~~~~~~~~~
In file included from ../../supervisor/shared/rgb_led_status.c:34:0:
../../shared-bindings/neopixel_write/__init__.h:35:13: note: declared here
 extern void common_hal_neopixel_write(const digitalio_digitalinout_obj_t* gpio, uint8_t *pixels, uint32_t numBytes, float brightness);
             ^~~~~~~~~~~~~~~~~~~~~~~~~
../../supervisor/shared/rgb_led_status.c: In function 'temp_status_color':
../../supervisor/shared/rgb_led_status.c:231:9: error: too few arguments to function 'common_hal_neopixel_write'
         common_hal_neopixel_write(&status_neopixel, colors, 3);
         ^~~~~~~~~~~~~~~~~~~~~~~~~
In file included from ../../supervisor/shared/rgb_led_status.c:34:0:
../../shared-bindings/neopixel_write/__init__.h:35:13: note: declared here
 extern void common_hal_neopixel_write(const digitalio_digitalinout_obj_t* gpio, uint8_t *pixels, uint32_t numBytes, float brightness);
             ^~~~~~~~~~~~~~~~~~~~~~~~~
../../supervisor/shared/rgb_led_status.c: In function 'clear_temp_status':
../../supervisor/shared/rgb_led_status.c:272:9: error: too few arguments to function 'common_hal_neopixel_write'
         common_hal_neopixel_write(&status_neopixel, status_neopixel_color, 3);
         ^~~~~~~~~~~~~~~~~~~~~~~~~
In file included from ../../supervisor/shared/rgb_led_status.c:34:0:
../../shared-bindings/neopixel_write/__init__.h:35:13: note: declared here
 extern void common_hal_neopixel_write(const digitalio_digitalinout_obj_t* gpio, uint8_t *pixels, uint32_t numBytes, float brightness);
             ^~~~~~~~~~~~~~~~~~~~~~~~~
```

It looks like other parts of the runtime call this function, so you need to update them to
call the new API, passing 1.0 as brightness. Try compiling again. You'll get another error, this one is
caused by binding function. Recall this is the  code that converts Python objects into C data types 
before invoking `common_hal_neopixel_write`:

```
partysaur:atmel-samd pal$ make BOARD=feather_m4_express
Use make V=1, make V=2 or set BUILD_VERBOSE similarly in your environment to increase build verbosity.
QSTR not updated
../../shared-bindings/neopixel_write/__init__.c: In function 'neopixel_write_neopixel_write_':
../../shared-bindings/neopixel_write/__init__.c:74:5: error: too few arguments to function 'common_hal_neopixel_write'
     common_hal_neopixel_write(digitalinout, (uint8_t*)bufinfo.buf, bufinfo.len);
     ^~~~~~~~~~~~~~~~~~~~~~~~~
In file included from ../../shared-bindings/neopixel_write/__init__.c:26:0:
../../shared-bindings/neopixel_write/__init__.h:35:13: note: declared here
 extern void common_hal_neopixel_write(const digitalio_digitalinout_obj_t* gpio, uint8_t *pixels, uint32_t numBytes, float brightness);
             ^~~~~~~~~~~~~~~~~~~~~~~~~
make: *** [build-feather_m4_express/shared-bindings/neopixel_write/__init__.o] Error 1
```

You need to modify this function (invoked from Python) so that it takes a brightness parameter
too. Since Python is dynamically typed, you can call this function with parameters of any type.
So notice how every parameter is just an `mp_obj_t`, which then the C code checks is the
right type. Add a third parameter for brightness. The function `mp_obj_is_float` tells you
whether an `mp_obj_t` is a float, while

`mp_float_t mp_obj_float_get(mp_obj_t self_in);`

transforms an `mp_obj_t` into an `mp_float_t`. You can just cast this to a float:

```
float bright = (float)mp_obj_float_get(param);
```

If you want to see other functions for manipulating MicroPython/CircuitPython types, check
out `py/obj.h`.

There's one last thing you have to do: you have to tell CircuitPython that 
`neopixel_write_neopixel_write_` takes three parameters, not just 2. Go back to 
`shared-bindings/neopixel_write/__init__.c` and change the line

`STATIC MP_DEFINE_CONST_FUN_OBJ_2(neopixel_write_neopixel_write_obj, neopixel_write_neopixel_write_);`

to

`STATIC MP_DEFINE_CONST_FUN_OBJ_3(neopixel_write_neopixel_write_obj, neopixel_write_neopixel_write_);`

If everything compiles, you're done! You've updated the CircuitPython runtime so that 
`neopixel_write` takes a third parameter, brightness. Install this new firmware. If
everything is working correctly, you should immediately get an error from Python:

```
Traceback (most recent call last):
  File "main.py", line 73, in <module>
  File "neopixel.py", line 230, in show
TypeError: function takes 3 positional arguments but 2 were given
```
You changed `neopixel_write` to take 3 arguments, but your Python code is calling it with 2.
Python detects this mismatch and throws an error. So, finally, we need to update the Python
library to pass brightness (rather than do the computation itself). Download 
[neopixel.py](neopixel.py). You installed this file in assignment 1, when you installed
the `libs` directory on your board. Go to the `show()` function and change it so it
just passes `self.brightness` to `neopixel_write`.

You need to compile your new `neopixel.py` into MicroPython bytecode (a `.mpy` file).
You can find the `mpy-cross` executable in `circuitpython/mpy-cross`. Compile
`neopixel.py` into `neopixel.mpy`. Copy your new `neopixel.mpy` to the `lib` folder
on `CIRCUITPY`. 

Now, all of your existing LED code should work, but you're no longer computing brightness
in Python.

  1. Write a loop that color-cycles 100 LEDs with a brightness of 0.5. How long does it
     take the loop to execute? How much faster is your implementation than the 
     original one?

### Fixes 1 and 2: Simplifying Python

The standard implementation of `__set_item` takes either an integer or a tuple holding
(R, G, B). Pick one, and trim down the function so it only accepts that type. 
Modify your `neopixel.py` to trim out all of the extra options and conditions. E.g.,
you only need to handle 3 LEDs (no white). You want as little Python code to execute
as possible. Play around with different programming approaches to see their performance
effects. As you did earlier, compile `neopixel.py` into `neopixel.mpy` with
`circuitpython/mpy-cross/mpy-cross` and copy `neopixel.mpy` to the `lib` directory
on CIRCUITPYTHON. When you're done:

  1. Write a loop that color-cycles 100 LEDs with a brightness of 0.5. How long does it
     take the loop to execute? How much faster is your implementation than the 
     original one?
  2. With larger API changes, you could probably make the API even faster. Suggest a set of
     more drastic changes which you hypothesize would improve performance even more.


### Handing in 

Please send an email to the staff list with subject "Assignment 3"
which has your answers to all of the above questions. Please send it
in the body of an email: do not send attachments, Google doc links,
etc. If you received significant help from someone besides the course
staff, please describe the help you received at the top of your
mail.

