# Assignment 2: Timing and Abstraction Boundaries

*Written by Philip Levis*

**Due: Tuesday, October 8, 2019 at 10:00 PM**

In the first assignment, you saw how the Feather can interact with
the physical world through light and a motor. You explored the
timing of these operations a little bit, and how the simplicity of
the Python interface increases timing imprecision. It's a bit like
always wearing oven mitts when baking: having them always on keeps
your hands safe and lets you quickly put things in and out of the oven,
but get in the way when you are trying to do precision work.

This is a common tradeoff in systems abstractions: high-level abstractions
are simple and easy, but can be slow or imprecise. The art of software
systems design (and 40 years of research) is designing abstractions
that give you the best of both worlds: they are simple to use *and*
have the performance of hand-tuned C code.

In this assignment, you'll explore an example problem which the Python
interface makes much more difficult. This motivates the next assignment,
in which you write a new driver for Python that gives you a better
abstraction of the LEDs, allowing you to easily solve the problem
in Python. 

There are questions interspersed with the assignment. You complete the
assignment by emailing answers to these questions to the course staff.

## Goals
When you're done with this assignment, you should

- understand the physical limitations of a servo,

- be familiar wth the interface a NeoPixel provides,

- start to have an intuition on Python's performance,

- be familiar with the Saleae and the timing of signals,

- understand why the standard CircuitPython driver for NeoPixels is so bad.

## Preliminaries 

In the previous assignment, you controlled LEDs and a servo motor. In this
assignment, you'll experience the challenges of doing both simultaneously.
You're going to change your servo code from assignment 1 so that, every 20ms,
it both sends a pulse to the motor *and* updates the 8 LEDs. 

1. How long do you think it takes to update the 8 LEDs?
1. How much of that time is assigning to pixels and how much of it is `pixels.show()`?

## Servo timing

In the previous assignment, you sent a pulse to the servo motor to center
it at 0 degrees. Now you're going to write a function that lets you set it
to any angle. Write a function `servo_angle(angle, time_ms)` that takes
an angle in the range of -90.0 to 90.0 and generates a pulse of the correct
duration every 20ms. The `time_ms` field denotes how long to send
this command for. For example, if `time_ms` is 500, then calling
`servo_angle` should send 25 pulses before returning.

1. Using the Saleae, look at your PWM signal. How precise can you get the 
PWM timing? For example, if you set the angle to -45, do you see a pulse of 1ms,
or 1.03ms?


## LED timing

Modify your servo loop so that, after sending the pulse, it updates
the 8 LEDs by stepping through the color wheel. Since there are 255 positions
and the pulse is 20ms, it should take approximately 5 seconds to cycle
through the colors.

Using the Saleae, take a look at the signal you're sending to the motor.
Does it still send a pulse every 20ms? If not, debug and adjust your code 
so that it does.

1. Describe how you wrote your code to keep the 20ms period while updating
the 8 LEDs.

### NeoPixel timing

Connect pin 1 of your Saleae to the signal pin for the NeoPixel. Be sure
to connect the ground too. Configure the Saleae so it will read both 
pin 0 (servo) and pin 1 (LED). Collect a data sample.

1. How long does it take to send the new pixel values to the 8 LEDs?
1. How long is the delay between your servo pulse and sending the new pixel values?

NeoPixels, like the servo motor, receive a pulse-width modulated (PWM) signal.
Each bit sent has the same duration, about 1.25 microseconds. A 0 bit is 
approximately 1/3 high and 2/3 low (400ns, 850ns), while a 1 bit is 
approximately 2/3 high and 1/3 low (800ns, 450ns).
To make it a little easier to read the signal, change your code so it only
programs one NeoPixel. Take another data sample and look at the bits that
are sent. Recall that before it can write new values to the NeoPixels,
the library must first send them a _clear_ command so they will listen to
new values.

1. What does the clear command look like?
1. How many bits does the Maxwell send to the NeoPixel to program it?
1. Are values transmitted as least-significant-bit (LSB) first or most-significant-bit (MSB) first? If you're not familiar with the difference between LSB and MSB, <a href="https://en.wikipedia.org/wiki/Bit_numbering">Wikipedia</a> has a good concise description.

### Larger LED arrays

Change your code so it programs 100 LEDs instead of 8. Since your LED
stick only has 8, the latter 92 commands will be ignored. But if, say,
each element of the art piece has 100 LEDs we'll want to be able to
program all of them. Be sure that you use a low brightness (e.g., 0.1)
so you can look at the LEDs.

1. How long does it take to set 100 LEDs?
1. What happens to the servo control when you set 100 LEDs?
1. Estimate how many LEDs `show()` can send commands to in 15ms. How many?

## Making it work

Devise a solution that allows you to correctly send a pulse to the motor
every 20ms even when you update many LEDs. Based on your answer to the
question above, there's a maximum you can update before `show()` will
cause the servo to slow down or misbehave. Try to get close to this limit.

1. How many LEDs can you control without missing the 20ms pulse deadline for the motor?
1. Describe your solution for controlling this number of LEDs.  Are your LED and servo code decoupled or tightly entwined?

## Where the time goes

The NeoPixel interface seems slow. Let's see where the time goes. Download
[neopixel.py](neopixel.py) and look at its source code. When you assign
to a pixel, Python calls the `__setitem__` function. 

You'll see that `show()` calls a function called `neopixel_write`.
This function is implemented in C code as part of a board's firmware,
and exported to Python. 

1. What fraction of `show()`'s execution time is spent in `neopixel_write`? Why?

Note that `show()` acts differently if your brightness is > 0.99. Change
your NeoPixel initialization to set `brightness=1` and rerun your code,
measuring how long `show()` takes before and after this change.

1. How much slower is `show()` when brightness is less than 0.99 versus
when it is 1?

Let's take a look at what `neopixel_write()` does. Open 
[__init__.c](__init__.c) for the SAM4D hardware driver for a NeoPixel.
This is the underlying C code. There's a separate file that does all of
the magic connecting Python with this code, which is mostly incomprehensible
structures that let C code act like Python, so we're not going to worry about 
it. If you want to see what it looks like, look at 
`shared-bindings/neopixel_write/__init__.c` in a CircuitPython distribution.

1. How does the SAM4D driver achieve the precise timing needed for pulses to NeoPixels?
1. Based on what you've seen with NeoPixel timing, think of a different
division of labor between Python and C that would be much faster. What does
the C API look like? How would this affect your Python interface? 
Describe each way your Python interface
differs from the existing one, if any. What underlying C functions does
the SAM4D provide in your design and how does the Python API map to them?


## Handing in

Please send an email to the staff list with subject "Assignment 2"
which has your answers to all of the above questions. Please send it
in the body of an email: do not send attachments, Google doc links,
etc. If you received significant help from someone besides the course
staff, please describe the help you received at the top of your
handin.

