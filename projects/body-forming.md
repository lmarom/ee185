# Project: Body Vacuum Forming

*Instructor Lead: Charles Gadeken*

**Due: Tuesday, January 28, 2020**

The goals and deliverables for this project are:
  1. Make the final mould for vacuum forming the body
  2. Add the CAD model for the mould and supporting CNC files to git
  3. Determine and document the vacuum forming material and process
  4. Determine the material and process for the connector and attaching it
  5. Determine the keyhole connectors that will be attached to the top plate
  6. Build 3 full bodies attached to top plates

## Make the final mould

A CAD model for the body is in git, but it requires some tweaks: Charlie
has the details. Refine and polish this CAD model, and make a mould from
it out of an appropriate material for vacuum forming. While we may make
more copies of the mould, this mould should be the final shape we are using.

Add all of the relevant files to the project git repository.

## Determine and document vacuum forming material and process

Decide what material, and thickness we will be using for the
body. You do not need to decide color, but if there are options that
Charlie would like to have then make sure the material supports them.
Develop, debug, and test the process for vacuum forming and
cutting-to-shape the outer shell. Document this process so someone
else skilled with the vacuum former and laser cutter (or other
cutting elements) can repeat the process without talking to you.

## Determine the material and process for the connector

The body has a rigid connector piece glued to it. This piece has the slots
for attachment to the top plate. There is a model for this piece in
the project git repository. Refine this model as needed based on any
changes to the body. Determine what material and thickness it will be
as well as the process for cutting it and attaching it to the body (glue,
set time, etc.), such that someone else can repeat the process.

## Determine exactly which keyhole connectors will be used

The current plan is to use keyhole connectors mounted on the top plate to
clip the body into place. Select the exact connectors that will be used.
If you conclude a different approach is needed, consult with Charlie first.

## Build 3 full bodies

Make top plates, attach the keyhole connectors, and clip on bodies. This
may involve changing the top plate model for the holes needed for the
keyhole connectors. If so, push those changes to the top plate model
in the project git repository, adding a suitable comment. This is
an end-to-end test of the models, process, and complete body.




