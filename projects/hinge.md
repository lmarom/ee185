# Project: Hinge

*Instructor Lead: Philip Levis*

**Due: Tuesday, February 4, 2020**

The goals and deliverables for this project are:
  1. Design the hinge mechanism for attaching wings to the body
  2. Verify the shaft and hinge mechanism can handle the motor torque
  3. Add the CAD and supporting files for the hinge to git
  4. Build 3 full hinge assemblies and test them
  5. Determine and document how the hinge parts will be sourced, order them


  

## Design the hinge mechanism

The hinge mechanism attaches the wing to the body top plate so that it can
travel up and down at least 180 degrees. The shaft in the hinge is actuated
by the mechanism motor assembly in the body. There is an existing hinge
design from the alpha prototype, but its shaft is far heavier than needed
and the hinge leaves do not fit well on the current body design.

A key aspect of the hinge design is its physical placement as well as
the placement of the bolts that hold the hinge leaves in place on the
wing and body. The body leaves need to extend far enough into the body
to clear the flange. The wing leaves need to give enough room for the
casing on the edge of the wing that holds the LEDs in place. The
placement of the leaves along the shaft must also give space for
the mechanism to rotate the shaft with its pulley system.

Designing the mechanism requires coordinating with the body and
mechanism groups. One major open question is what plane the shaft is
on: is it parallel to the top plate, above it, or below it?

The top plate and casings of around the edges of the wings are both
going to be a silver/steel/aluminum color. The hinge should be too,
unless Charlie makes an artistic decision for some contrast. Check
with him.

## Verify hinge can handle motor torque

The motor is going to apply significant torque in order to move the
wing. This is because the wheel that the cable attaches to is quite
small: there is very little mechanical advantage, so the motor must
apply a lot of force. If the shaft or hinge mechanism is not strong
enough, they will bend or break.

Work with the mechanism group to be sure that the hinge will be able
to support the forces on it. This will in part require determining the
material of the hinge parts. 3D printed plastic hinge leaves will need
to look very different that milled aluminum or water-jet steel.

## Add CAD files to git

If you need help with this, work with Matt.

## Build 3 full hinge assemblies and test them

Attach them to top plates and wings with casing. If possible, attach
mechanisms to them. Test the mechanisms and the hinges for at least 24
hours.  Leave one running forever, as a long-running test.

## Determine and document sourcing of hinge parts

Talk with Charlie, Mark, Steve, and Phil about different options.
Look into each of them and suggest a course of action. Check this
by the instructions. Once the sourcing is decided and we have verified
they meet all of our engineering requirements (durability, cost, weight,
fit, etc.), order the parts.





