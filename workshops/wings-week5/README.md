# Workshop: Wing Etchings

**Date: Monday, February 3, 2020**

## Goals
In this workshop, we'll take the collection of fractal designs for
Fractal Flyer wings and distill them down to a few that we'll etch
and look at.

## Introduction: The Wings 

Each Fractal Flyer is approximately 26" long and 22" wide. This is what
its profile looks like from above:

![Shape top profile](wings-and-body.png)

The red lines are the outlines of separate pieces. In this figure, the
two wings have blue rays on them: these are etched patterns that will
cast shadows in the day and light up at night. 

![Wing lit up](wing-light.jpg)

Last week's workshop
and class came up with a collection of potential etching patterns based
on fractals (Julia sets and the Mandelbrot set).  Today's workshop is 
about picking a few of these patterns and adding embellishments that
reflect the breadth of the EE department.

## Designing a Wing Pattern 


Download the existing [wing design](https://code.stanford.edu/plevis/ee185/raw/master/cad/Wings/wings-only.svg?inline=false).
Open it in Inkscape. 

This 
[shared Google Drive](https://drive.google.com/drive/folders/1etVBdh9Bf1vp4Tx4XnBBn0loK7iz9--S?usp=sharing)
has a collection of potential etching patterns. Look through all of the designs
and pick one that you particularly like. You now need to take this image and turn it
into a black and white design which you will put on the wing. This may require
cutting away black regions in the pattern, or otherwise changing the colors of
the picture. Import it into Inkscape, and scale it in size so
it fits on the wing and covers much of its surface. This will likely be an iterative process:
after putting the design on a wing, you'll realize you want to change the design a little,
then put it on the wing again.

Be sure that there is a region on the wing that does not have any etching. 
In addition to these fractal patterns, each wing will have a small embellishment
consisting of either a few words or a small picture. These embellishments represent
the diversity of the EE department.

## Adding an Embellishment 

Now that you have your design, add one of the following three embellishments:

 - [A NAND gate](nand.svg), the logic gate from which one can create any circuit. This represents the intellectual fields of logic, digital design, and computer architecture. 
 - [The Stanford Student Space Initiative logo](sssi.svg). This represents how aerospace uses Electrical Engineering in many ways (circuits, signals, control) as well as how EE students are engaged in SSSI. 
 - [A multi-armed bandit](bandit_bw.png). This represents sampling theory, and how mathematical principles show us there are ways to get much more accurate answers than asking random questions. 


## Make a Wing

Once you have a final SVG file with your etching pattern (both the design and embellishment), check that they are in black.
Save your SVG and save it to the [shared Google Drive](https://drive.google.com/drive/folders/1etVBdh9Bf1vp4Tx4XnBBn0loK7iz9--S?usp=sharing),
naming it something representative. Then, cut it on the laser cutter!



