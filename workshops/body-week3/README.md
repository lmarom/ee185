# Workshop: Body Building 

**Date: Wednesday, January 22, 2020**

## Goals
In this workshop, we'll 

- see all the structural pieces that make up a bird
- assemble 4 mock-ups of the bird's body shell
- assemble one full bird

## Introduction: The Installation at Large and Small Scale

The art installation will have 50-100 lit, iridescent geometric
shapes hanging in the large glass Packard stairwell:

![Packard stairwell](stairwell.jpg)

We do not yet know the exact placement of the shapes. But the 
current plan is to have them in an arc that starts at the 
top right corner of the white wall in the stairwell, arcs
counterclockwise and down, so it touches the front glass about
halfway down, then continues to finish at the bottom left
corner of the front glass wall (from inside, so the corner
right by the front doors).

Each shape is approximately 26" long and 22" wide. This is what
its profile looks like from above:

![Shape top profile](wings-and-body.png)

The red lines are the outlines of separate pieces. The center is the body: 
it is a diamond shape that is internally lit. The two triangles are the 
wings, individually moving pieces that rotate on the axis parallel to 
the body. The wings are plexiglass treated with dichroic film, so they 
change in color as the angle of light through them changes. The blue lines
are etchings on the plexiglass, which LEDs on the edges of the wings
light up.

### Body

The body consists of three major parts. The *top plate* is the diamond
shape in the above figure; it is the structural element on which everything
else hangs. In the final piece it will likely be stainless steel. The *shell*
is a thin, shaped plastic piece that makes the 3D form of the body. It looks
like this:

![Body shell](shell.png)

The third and final piece is the *flange*. This is a piece that is glued to
the shell and allows the shell to attach to the top plate. The top plate
has four screws in it, whose heads hang down from the top plate (they
are not flush). The flange has keyholes: holes big enough for the head of
the screw, with a thin slot extending from these holes. So you position
the shell so the screws go through the holes, then slide it forward to lock
it into place. Here's a picture of the flange:

![Flange](flange.png)

The body shell is going to be an opaque plastic that allows most light
to pass through it: this will allow us to internally light the body 
with LEDs.

### Wings

Each body has two wings attached to it. These wings are controlled by a
mechanism and small computer inside the body. The wings rotate around
a shaft that's connected both to the wing and top plate. Each wing is
surrounded by LEDs that shine inside of it. The properties of light
in the plexiglass of the wings means most of the light stays inside
the wing, except where it's etched:

![Lit up wing](wing-light.jpg)

You'll notice that the etching pattern has an empty box in it. This
is going to be a region in across which pairs of wings vary. The images,
icons, or illustrations in this region are going to be collected from
contributions from the EE department, to express the diversity of its
people, interests, and accomplishments.

## Workshop Goals

In this workshop, we are going to build three bodies and one full shape
(we only have enough connectors and shafts for one pair of wings). The
first step is making the shells, because they will take some time to
build and set into shape.

### Building 4 bodies

In the final art piece, the shells will be vacuum-formed. In vacuum
forming, you first create a mould for the shape. You place this inside
the vacuum chamber, which sucks air out from the bottom, where the mold
rests. You put a sheet of the material you wish to form inside, and cover
it with a rubber sheet that weighs it down. You then heat the chamber: the
material melts, and forms down onto the mould.

We're in the process of making this mold and finalizing the vacuum forming
workflow, so the shells we're making today will be prototypes out of sheets
of acrylic that we'll glue together.  Each body shell should have four pieces,
which you'll glue together. To hold them in the right geometry as we glue
them, you'll use a flange.

The basic steps for assembling a shell are as follows. Please read all of the
steps before getting started, so you understand where the process is going.
Take things slow -- this kind of build process is much more successful if you
don't rush; otherwise it might fall apart at the end. If you don't understand
one of the steps, please check in with Steve, Charlie, or Phil.

1. Tape a flange to a table top. The flange provides a rigid shape to place the
   shell panels around, and taping it prevents it from walking around.
1. Place the shell panels together on the flange, holding them in place with a bit of 
   tape.
1. Hot glue / acrylic glue the joints between the panels in a civilized manner: 
   you don't need a lot. Be sure you don't glue them to the flange (that's later).
1. Let the glue cool/set.
1. Take the shell off the flange.
1. Remove the tape on the flange so it can be positioned into the shell.
1. Tape the flange to inside of the the shell where it belongs. The flange should
   be flush with the top edge of the shell.
1. Hot glue the upper edge of the flange in a couple spots. Make sure that the 
   edge of the flange sits nicely along the panel inside surface.
1. Apply a bead of acrylic cement to the flange/shell joint. Do not use hot glue here.
1. Let the glue cool/set.

At this point, you have nice shell with an attached flange. Now you just need
to attach it to a top plate:

1. Take a top plate and four screws/bolts.
1. Find the four holes in the top plate that line up with the keyholes in the flange, and
   put in screws/bolts. Be sure that the heads are evenly spaced away from
   the top plate, using a caliper if need be. Also make sure that the
   heads are set away far enough that the will be able to go through the flange
   holes: if they are too close them the shell will not slide over the top plate.
1. Find the fifth hole for the pin that will hold the shell in place and stop it
   from sliding back and forth: this is a pin that just goes through the top plate
   into the flange.
1. Slide the shell onto the top plate.
1. Insert the pin. 

### Building a Shape

Now that we have our bodies, we're going to build one pair of wings, attach them,
and light them up.

Start with the body that seems the sturdiest. We'll need to do five things:

1. Attach connectors onto the body which the shaft can pass through.
1. Put a string of LEDs along the edge of each wing. Tape it into place
   with black tape. Be sure that the female socket (the input) is along
   the edge adjacent to the body, as this is where power and data will
   come from. Be sure the tape does not cover up the holes needed for
   the connector.
1. Connect a Feather board to the LED strip to make sure it's lighting
   up correctly. You'll want to provide 5V to the red wire, ground to
   the white, and data (from the board) to green. *Fill in details here
   before workshop.*
1. Attach connectors onto the wing. Be sure to use the set screw so
   the wing doesn't rotate along the shaft. 
1. Attach the wing to the body with the shaft. Use tape or some other
   material to keep it aloft.
  


 
